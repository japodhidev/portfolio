<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Request;


use App\Entity\User;
use App\Form\UserType;
use Symfony\Component\Security\Core\User\UserInterface;

class RegistrationController extends AbstractController
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }
    /**
     * @Route("/registration", name="registration")
     */
//    public function index()
//    {
//        return $this->render('registration/index.html.twig', [
//            'controller_name' => 'RegistrationController',
//        ]);
//    }

    /**
     * @Route("/user/register", name="register")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function register(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $email = $form->get('email')->getData();
            $pwd = $form->get('password')->getData();

            $user->setPassword($this->passwordEncoder->encodePassword(
                $user, $pwd
            ));

            $entityHandle = $this->getDoctrine()->getManager();
            $entityHandle->persist($user);
            try {
                $entityHandle->flush();
            } catch (\Exception $e){
                return $this->render('Exception/registration-error.html.twig',[
                    'title' => 'Registration Error',
                    'error_msg' => "The email ". $email. " is already in use. Please provide a different one to continue"
                ]);
            }

            // Send registration successful email

            return $this->redirect('login');

        }

        return $this->render('auth/register.html.twig', [
            'title' => 'Register',
            'registrationForm' => $form->createView()
        ]);

    }

}
