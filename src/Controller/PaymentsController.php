<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Payments\MpesaPayments;

class PaymentsController extends AbstractController
{

	/**
	 * @Route("/payments/mpesa/initiate", name="mpesa_initiate")
	 */
	public function mpesaInitiate(){
		$mpesa = new MpesaPayments();
		$response = $mpesa->registerUrl();

		return $this->json($response);
	}

	/**
	 * @Route("/payments/mpesa/confirmation", name="mpesa_confirmation")
	 */
	
	public function mpesaConfirmation(){
		return $this->json(['message' => 'mpesa confirmation']);
	}

	/**
	 * @Route("/payments/mpesa/validate", name="mpesa_validation")
	 */
	
	public function mpesaValidation(){
		return $this->json(['message' => 'Mpesa validation']);
	}

}