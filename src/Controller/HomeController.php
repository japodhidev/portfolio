<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Payments\MpesaPayments;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="default")
     */
    public function index()
    {
        return $this->render('home/index.html.twig', [
            'title' => 'Japodhi\'s Portfolio',
        ]);
    }

    /**
     * @Route("/home", name="home")
     */
    public function home()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        /** @var \App\Entity\User $user */
        $user = $this->getUser();

        return $this->render('home/home.html.twig', [
            'title' => 'Japodhi\'s Portfolio - Homepage',
            'name' => $user->getFirstName()
        ]);

    }

    /**
     * @Route("/test", name="test")
     *
     */
    public function test() {
        $test = new MpesaPayments();
        $response = $test->getAccessToken();

        return $this->json($response);

        /*return $this->render('home/test.html.twig', [
            'title' => 'Test Page'
        ]);*/
    }

    /**
     * @Route("/user", name="users")
     */

    public function users()
    {
        return $this->render('auth/user.html.twig', [
            'title' => 'Portfolio - Dashboard'
        ]);
    }

    /**
     * @Route("/bio", name="bio")
     */

    public function bio(){
        return $this->render('home/bio.html.twig', [
            'title' => 'Japodhi\'s - Bio'
        ]);
    }


}
