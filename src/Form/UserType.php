<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;


class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'constraints' => ([
                    new NotBlank([
                        'message' => 'An empty email field is not allowed.'
                    ])
                ]),
                'attr' => ['class' => 'form-control'],
                'label' => 'Email'
            ])
            ->add('firstname', TextType::class, [
                'constraints' => ([
                    new NotBlank([
                        'message' => 'An empty name field is not allowed.'
                    ]),
                ]),
                'attr' => ['class' => 'form-control'],
                'label' => 'First Name'
            ])
            ->add('secondname', TextType::class, [
                'constraints' => ([
                    new NotBlank([
                        'message' => 'An empty name field is not allowed.'
                    ]),
                ]),
                'attr' => ['class' => 'form-control'],
                'label' => 'Second Name'
            ])
            ->add('password', PasswordType::class, [
                'mapped' => false,
                'constraints' => ([
                    new NotBlank([
                        'message' => 'Please enter a password'
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Your password should be at least {{ limit }} characters',
                        'max' => 4096
                    ]),
                ]),
                'attr' => ['class' => 'form-control'],
                'label' => 'Password'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
