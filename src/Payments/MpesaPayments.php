<?php

namespace App\Payments;

/**
 * 
 */
class MpesaPayments
{

	public function getAccessToken() {
		$url = 'https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        $creds = base64_encode('DjPl5Bu04YCgTKUoNI8qTpM4z1hMDpK0:9FhZAhtBITeiGkJK');
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Authorization: Basic ' . $creds
        ));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $curl_response = curl_exec($curl);
        // $pretty = preg_replace('/\s\S+/', '', $curl_response);
        // preg_match('/(\{.*\})/', $pretty, $pretty_response);

        return $curl_response;
	}

	/**
	 * [registerUrl description]
	 * @return [json] [json response]
	 */
	
	public function registerUrl(){

		$url = 'https://sandbox.safaricom.co.ke/mpesa/c2b/v1/registerurl';

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Authorization:Bearer 8ATjeGZtpHAIEdt0HUQ8oRq7SPCZ'
		));

		$curl_post_data = array(
			'ShortCode' => '601334',
			'ResponseType' => '0',
			'ConfirmationURL' => 'http://portfolio.com/payments/mpesa/confirmation',
			'ValidationURL' => 'http://portfolio.com/payments/mpesa/validate'
		);

		$data_json = json_encode($curl_post_data);

		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data_json);

		$response = curl_exec($curl);
		return $response;
	}
}